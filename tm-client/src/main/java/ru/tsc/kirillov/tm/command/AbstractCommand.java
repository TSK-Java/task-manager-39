package ru.tsc.kirillov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.model.ICommand;
import ru.tsc.kirillov.tm.api.service.IServiceLocator;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.exception.system.CommandNotRegisteredException;

public abstract class AbstractCommand implements ICommand {

    @Nullable
    protected IServiceLocator serviceLocator;

    @Nullable
    public abstract String getName();

    @Nullable
    public String getArgument() {
        return null;
    }

    @Nullable
    public abstract String getDescription();

    public abstract void execute();

    @Nullable
    public abstract Role[] getRoles();

    @Nullable
    protected String getToken() {
        return getServiceLocator().getTokenService().getToken();
    }

    protected void setToken(@Nullable final String token) {
        getServiceLocator().getTokenService().setToken(token);
    }

    public void setServiceLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public IServiceLocator getServiceLocator() {
        if (serviceLocator == null) throw new CommandNotRegisteredException();
        return serviceLocator;
    }

    @Override
    public String toString() {
        @NotNull String result = "";
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();

        if (name != null && !name.isEmpty())
            result += name;
        if (argument != null && !argument.isEmpty())
            result += ", " + argument;
        if (description != null && !description.isEmpty())
            result += " - " + description;

        if (!result.isEmpty())
            return result;
        else
            return super.toString();
    }

}
