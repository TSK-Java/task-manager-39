package ru.tsc.kirillov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.dto.request.TaskUpdateByIndexRequest;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-update-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Обновить задачу по её индексу.";
    }

    @Override
    public void execute() {
        System.out.println("[Обновление задачи по индексу]");
        System.out.println("Введите индекс задачи:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        System.out.println("Введите имя:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIndexRequest request =
                new TaskUpdateByIndexRequest(getToken(), index, name, description);
        getTaskEndpoint().updateTaskByIndex(request);
    }

}
