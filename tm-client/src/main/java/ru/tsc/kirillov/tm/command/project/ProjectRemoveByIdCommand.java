package ru.tsc.kirillov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.request.ProjectRemoveByIdRequest;
import ru.tsc.kirillov.tm.dto.response.ProjectRemoveByIdResponse;
import ru.tsc.kirillov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectTaskCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-remove-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удалить проект по его ID.";
    }

    @Override
    public void execute() {
        System.out.println("[Удаление проекта по ID]");
        System.out.println("Введите ID проекта:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(getToken(), id);
        @NotNull ProjectRemoveByIdResponse response = getProjectTaskEndpoint().removeProjectById(request);
        @Nullable final Project project = response.getProject();
        if (project == null) throw new ProjectNotFoundException();
    }

}
