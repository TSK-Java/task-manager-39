package ru.tsc.kirillov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.request.ProjectListRequest;
import ru.tsc.kirillov.tm.dto.response.ProjectListResponse;
import ru.tsc.kirillov.tm.enumerated.Sort;
import ru.tsc.kirillov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отображение списка проектов.";
    }

    @Override
    public void execute() {
        System.out.println("[Список проектов]");
        System.out.println("Введите способ сортировки");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken(), sort);
        @NotNull final ProjectListResponse response = getProjectEndpoint().listProject(request);
        @Nullable final List<Project> projects = response.getProjects();
        if (projects == null) throw new ProjectNotFoundException();
        int idx = 0;
        for(final Project project: projects) {
            if (project == null) continue;
            System.out.println(++idx + ". " + project);
        }
    }

}
