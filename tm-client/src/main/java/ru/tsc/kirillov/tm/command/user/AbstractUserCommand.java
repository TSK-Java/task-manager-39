package ru.tsc.kirillov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.endpoint.IUserEndpoint;
import ru.tsc.kirillov.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected IUserEndpoint getUserEndpoint() {
        return getServiceLocator().getUserEndpoint();
    }

}
