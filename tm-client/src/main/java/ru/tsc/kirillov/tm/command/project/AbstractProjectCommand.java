package ru.tsc.kirillov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.kirillov.tm.command.AbstractCommand;
import ru.tsc.kirillov.tm.enumerated.Role;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    public IProjectEndpoint getProjectEndpoint() {
        return getServiceLocator().getProjectEndpoint();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
