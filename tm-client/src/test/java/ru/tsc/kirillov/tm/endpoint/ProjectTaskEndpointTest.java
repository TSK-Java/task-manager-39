package ru.tsc.kirillov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.kirillov.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.kirillov.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.kirillov.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.kirillov.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.kirillov.tm.api.service.IPropertyService;
import ru.tsc.kirillov.tm.dto.request.*;
import ru.tsc.kirillov.tm.dto.response.*;
import ru.tsc.kirillov.tm.marker.ISoapCategory;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.model.Task;
import ru.tsc.kirillov.tm.service.PropertyService;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Category(ISoapCategory.class)
public final class ProjectTaskEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService);

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = IProjectTaskEndpoint.newInstance(propertyService);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @Nullable
    private String tokenTest;

    @NotNull
    private final String invalid = UUID.randomUUID().toString();

    @NotNull
    private final String taskName = UUID.randomUUID().toString();

    @NotNull
    private final String taskDescription = UUID.randomUUID().toString();

    @NotNull
    private final String projectName = UUID.randomUUID().toString();

    @NotNull
    private final String projectDescription = UUID.randomUUID().toString();

    @NotNull final Date dateBegin = new Date();

    @NotNull final Date dateEnd = new Date();

    @Before
    public void initialization() {
        @NotNull final UserLoginResponse response = authEndpoint.login(
                new UserLoginRequest("test", "test")
        );
        tokenTest = response.getToken();
        taskEndpoint.clearTask(new TaskClearRequest(tokenTest));
        projectTaskEndpoint.clearProject(new ProjectClearRequest(tokenTest));
    }

    @After
    public void finalization() {
        taskEndpoint.clearTask(new TaskClearRequest(tokenTest));
        projectTaskEndpoint.clearProject(new ProjectClearRequest(tokenTest));
        tokenTest = null;
    }

    @NotNull
    private String prepareData() {
        @NotNull TaskCreateResponse responseTask = taskEndpoint.createTask(
                new TaskCreateRequest(tokenTest, taskName, taskDescription, dateBegin, dateEnd)
        );
        Assert.assertNotNull(responseTask);
        @Nullable Task task = responseTask.getTask();
        Assert.assertNotNull(task);
        @NotNull ProjectCreateResponse responseProject = projectEndpoint.createProject(
                new ProjectCreateRequest(tokenTest, projectName, projectDescription, dateBegin, dateEnd)
        );
        Assert.assertNotNull(responseProject);
        @Nullable Project project = responseProject.getProject();
        Assert.assertNotNull(project);
        projectTaskEndpoint.bindTaskToProjectId(new ProjectBindTaskByIdRequest(tokenTest, project.getId(), task.getId()));
        projectEndpoint.createProject(
                new ProjectCreateRequest(tokenTest, projectName, projectDescription, dateBegin, dateEnd)
        );
        responseTask = taskEndpoint.createTask(
                new TaskCreateRequest(tokenTest, taskName, taskDescription, dateBegin, dateEnd)
        );
        Assert.assertNotNull(responseTask);
        task = responseTask.getTask();
        Assert.assertNotNull(task);

        return project.getId();
    }

    @Nullable
    private List<Task> getTasks() {
        @NotNull TaskListResponse responseList = taskEndpoint.listTask(new TaskListRequest(tokenTest));
        Assert.assertNotNull(responseList);
        return responseList.getTasks();
    }

    @Nullable
    private List<Project> getProjects() {
        @NotNull ProjectListResponse response = projectEndpoint.listProject(new ProjectListRequest(tokenTest));
        Assert.assertNotNull(response);
        return response.getProjects();
    }

    @Test
    public void clearProject() {
        prepareData();
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.clearProject(
                        new ProjectClearRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.clearProject(
                        new ProjectClearRequest(null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.clearProject(
                        new ProjectClearRequest("")
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.clearProject(
                        new ProjectClearRequest(invalid)
                )
        );
        @Nullable ProjectClearResponse responseClear = projectTaskEndpoint.clearProject(
                new ProjectClearRequest(tokenTest)
        );
        Assert.assertNotNull(responseClear);
        @Nullable final List<Task> tasks = getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        Assert.assertNull(getProjects());
    }

    @Test
    public void removeProjectById() {
        @NotNull String projectId = prepareData();
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.removeProjectById(
                        new ProjectRemoveByIdRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.removeProjectById(
                        new ProjectRemoveByIdRequest(null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.removeProjectById(
                        new ProjectRemoveByIdRequest("", null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.removeProjectById(
                        new ProjectRemoveByIdRequest(invalid, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.removeProjectById(
                        new ProjectRemoveByIdRequest(tokenTest, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.removeProjectById(
                        new ProjectRemoveByIdRequest(tokenTest, "")
                )
        );
        @Nullable List<Task> tasks = getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        @Nullable List<Project> projects = getProjects();
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());

        @Nullable ProjectRemoveByIdResponse response = projectTaskEndpoint.removeProjectById(
                new ProjectRemoveByIdRequest(tokenTest, projectId)
        );
        Assert.assertNotNull(response);

        tasks = getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        projects = getProjects();
        Assert.assertNotNull(projects);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void removeProjectByIndex() {
        prepareData();
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.removeProjectByIndex(
                        new ProjectRemoveByIndexRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.removeProjectByIndex(
                        new ProjectRemoveByIndexRequest(null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.removeProjectByIndex(
                        new ProjectRemoveByIndexRequest("", null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.removeProjectByIndex(
                        new ProjectRemoveByIndexRequest(invalid, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.removeProjectByIndex(
                        new ProjectRemoveByIndexRequest(tokenTest, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.removeProjectByIndex(
                        new ProjectRemoveByIndexRequest(tokenTest, -50)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.removeProjectByIndex(
                        new ProjectRemoveByIndexRequest(tokenTest, 50)
                )
        );
        @Nullable List<Task> tasks = getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        @Nullable List<Project> projects = getProjects();
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());

        @Nullable ProjectRemoveByIndexResponse response = projectTaskEndpoint.removeProjectByIndex(
                new ProjectRemoveByIndexRequest(tokenTest, 1)
        );
        Assert.assertNotNull(response);

        tasks = getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        projects = getProjects();
        Assert.assertNotNull(projects);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void bindTaskToProjectId() {
        @NotNull TaskCreateResponse responseTask = taskEndpoint.createTask(
                new TaskCreateRequest(tokenTest, taskName, taskDescription, dateBegin, dateEnd)
        );
        Assert.assertNotNull(responseTask);
        @Nullable Task task = responseTask.getTask();
        Assert.assertNotNull(task);
        @NotNull ProjectCreateResponse responseProject = projectEndpoint.createProject(
                new ProjectCreateRequest(tokenTest, projectName, projectDescription, dateBegin, dateEnd)
        );
        Assert.assertNotNull(responseProject);
        @Nullable Project project = responseProject.getProject();
        Assert.assertNotNull(project);

        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.bindTaskToProjectId(
                        new ProjectBindTaskByIdRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.bindTaskToProjectId(
                        new ProjectBindTaskByIdRequest(null, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.bindTaskToProjectId(
                        new ProjectBindTaskByIdRequest("", "", "")
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.bindTaskToProjectId(
                        new ProjectBindTaskByIdRequest(invalid, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.bindTaskToProjectId(
                        new ProjectBindTaskByIdRequest(tokenTest, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.bindTaskToProjectId(
                        new ProjectBindTaskByIdRequest(tokenTest, "", null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.bindTaskToProjectId(
                        new ProjectBindTaskByIdRequest(tokenTest, project.getId(), null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.bindTaskToProjectId(
                        new ProjectBindTaskByIdRequest(tokenTest, project.getId(), "")
                )
        );

        projectTaskEndpoint.bindTaskToProjectId(new ProjectBindTaskByIdRequest(tokenTest, project.getId(), task.getId()));

        projectEndpoint.createProject(
                new ProjectCreateRequest(tokenTest, projectName, projectDescription, dateBegin, dateEnd)
        );
        taskEndpoint.createTask(
                new TaskCreateRequest(tokenTest, taskName, taskDescription, dateBegin, dateEnd)
        );

        @Nullable List<Task> tasks = getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        @Nullable List<Project> projects = getProjects();
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
        @Nullable TaskListByProjectIdResponse response = taskEndpoint.listTaskByProjectId(
                new TaskListByProjectIdRequest(tokenTest, project.getId())
        );
        Assert.assertNotNull(response);
        tasks = response.getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        Assert.assertEquals(tasks.get(0).getId(), task.getId());
    }

    @Test
    public void unbindTaskToProjectId() {
        @NotNull TaskCreateResponse responseTask = taskEndpoint.createTask(
                new TaskCreateRequest(tokenTest, taskName, taskDescription, dateBegin, dateEnd)
        );
        Assert.assertNotNull(responseTask);
        @Nullable Task task = responseTask.getTask();
        Assert.assertNotNull(task);
        @NotNull ProjectCreateResponse responseProject = projectEndpoint.createProject(
                new ProjectCreateRequest(tokenTest, projectName, projectDescription, dateBegin, dateEnd)
        );
        Assert.assertNotNull(responseProject);
        @Nullable Project project = responseProject.getProject();
        Assert.assertNotNull(project);

        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.unbindTaskToProjectId(
                        new ProjectUnbindTaskByIdRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.unbindTaskToProjectId(
                        new ProjectUnbindTaskByIdRequest(null, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.unbindTaskToProjectId(
                        new ProjectUnbindTaskByIdRequest("", "", "")
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.unbindTaskToProjectId(
                        new ProjectUnbindTaskByIdRequest(invalid, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.unbindTaskToProjectId(
                        new ProjectUnbindTaskByIdRequest(tokenTest, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.unbindTaskToProjectId(
                        new ProjectUnbindTaskByIdRequest(tokenTest, "", null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.unbindTaskToProjectId(
                        new ProjectUnbindTaskByIdRequest(tokenTest, project.getId(), null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.unbindTaskToProjectId(
                        new ProjectUnbindTaskByIdRequest(tokenTest, project.getId(), "")
                )
        );

        projectTaskEndpoint.bindTaskToProjectId(new ProjectBindTaskByIdRequest(tokenTest, project.getId(), task.getId()));

        projectEndpoint.createProject(
                new ProjectCreateRequest(tokenTest, projectName, projectDescription, dateBegin, dateEnd)
        );
        taskEndpoint.createTask(
                new TaskCreateRequest(tokenTest, taskName, taskDescription, dateBegin, dateEnd)
        );

        @Nullable List<Task> tasks = getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        @Nullable List<Project> projects = getProjects();
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
        @Nullable TaskListByProjectIdResponse response = taskEndpoint.listTaskByProjectId(
                new TaskListByProjectIdRequest(tokenTest, project.getId())
        );
        Assert.assertNotNull(response);
        tasks = response.getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        Assert.assertEquals(tasks.get(0).getId(), task.getId());

        projectTaskEndpoint.unbindTaskToProjectId(new ProjectUnbindTaskByIdRequest(tokenTest, project.getId(), task.getId()));

        tasks = getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        projects = getProjects();
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
        response = taskEndpoint.listTaskByProjectId(
                new TaskListByProjectIdRequest(tokenTest, project.getId())
        );
        Assert.assertNotNull(response);
        tasks = response.getTasks();
        Assert.assertNull(tasks);
    }

}
