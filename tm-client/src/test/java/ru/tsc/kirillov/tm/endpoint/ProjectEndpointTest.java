package ru.tsc.kirillov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.kirillov.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.kirillov.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.kirillov.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.kirillov.tm.api.service.IPropertyService;
import ru.tsc.kirillov.tm.dto.request.*;
import ru.tsc.kirillov.tm.dto.response.*;
import ru.tsc.kirillov.tm.enumerated.Sort;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.marker.ISoapCategory;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Category(ISoapCategory.class)
public final class ProjectEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = IProjectTaskEndpoint.newInstance(propertyService);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @Nullable
    private String tokenTest;

    @NotNull
    private final String invalid = UUID.randomUUID().toString();

    @NotNull
    private final String projectName = UUID.randomUUID().toString();

    @NotNull
    private final String projectDescription = UUID.randomUUID().toString();

    @NotNull final Date dateBegin = new Date();

    @NotNull final Date dateEnd = new Date();

    @Before
    public void initialization() {
        @NotNull final UserLoginResponse response = authEndpoint.login(
                new UserLoginRequest("test", "test")
        );
        tokenTest = response.getToken();
        projectTaskEndpoint.clearProject(new ProjectClearRequest(tokenTest));
    }

    @After
    public void finalization() {
        projectTaskEndpoint.clearProject(new ProjectClearRequest(tokenTest));
        tokenTest = null;
    }

    @Test
    public void createProject() {
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.createProject(
                        new ProjectCreateRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.createProject(
                        new ProjectCreateRequest("", "", "", null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.createProject(
                        new ProjectCreateRequest(null, "", "", null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.createProject(
                        new ProjectCreateRequest(tokenTest, "", "", null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.createProject(
                        new ProjectCreateRequest(invalid, "", "", null, null)
                )
        );
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
            new ProjectCreateRequest(tokenTest, projectName, projectDescription, dateBegin, dateEnd)
        );
        Assert.assertNotNull(response);
        @Nullable Project project = response.getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(projectName, project.getName());
        Assert.assertEquals(projectDescription, project.getDescription());
        Assert.assertEquals(dateBegin.toString(), project.getDateBegin().toString());
        Assert.assertEquals(dateEnd.toString(), project.getDateEnd().toString());
    }

    @Test
    public void changeProjectStatusById() {
        @NotNull final ProjectCreateResponse responseProject = projectEndpoint.createProject(
            new ProjectCreateRequest(tokenTest, projectName, projectDescription, dateBegin, dateEnd)
        );
        Assert.assertNotNull(responseProject);
        @Nullable Project project = responseProject.getProject();
        Assert.assertNotNull(project);
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(null, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest("", null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(invalid, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(tokenTest, invalid, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(tokenTest, project.getId(), null)
                )
        );
        ProjectChangeStatusByIdResponse response = projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(tokenTest, project.getId(), Status.COMPLETED)
        );
        Assert.assertNotNull(response);
        @Nullable Project projectChanged = response.getProject();
        Assert.assertNotNull(projectChanged);
        Assert.assertNotEquals(project.getStatus(), Status.COMPLETED);
        Assert.assertNotEquals(project.getStatus(), projectChanged.getStatus());
        Assert.assertEquals(projectChanged.getStatus(), Status.COMPLETED);
    }

    @Test
    public void changeProjectStatusByIndex() {
        @NotNull final ProjectCreateResponse responseProject = projectEndpoint.createProject(
                new ProjectCreateRequest(tokenTest, projectName, projectDescription, dateBegin, dateEnd)
        );
        Assert.assertNotNull(responseProject);
        @Nullable Project project = responseProject.getProject();
        Assert.assertNotNull(project);
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusByIndex(
                        new ProjectChangeStatusByIndexRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusByIndex(
                        new ProjectChangeStatusByIndexRequest(null, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusByIndex(
                        new ProjectChangeStatusByIndexRequest("", null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusByIndex(
                        new ProjectChangeStatusByIndexRequest(invalid, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusByIndex(
                        new ProjectChangeStatusByIndexRequest(tokenTest, -50, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusByIndex(
                        new ProjectChangeStatusByIndexRequest(tokenTest, 50, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusByIndex(
                        new ProjectChangeStatusByIndexRequest(tokenTest, 1, null)
                )
        );
        ProjectChangeStatusByIndexResponse response = projectEndpoint.changeProjectStatusByIndex(
                new ProjectChangeStatusByIndexRequest(tokenTest, 1, Status.COMPLETED)
        );
        Assert.assertNotNull(response);
        @Nullable Project projectChanged = response.getProject();
        Assert.assertNotNull(projectChanged);
        Assert.assertNotEquals(project.getStatus(), Status.COMPLETED);
        Assert.assertNotEquals(project.getStatus(), projectChanged.getStatus());
        Assert.assertEquals(projectChanged.getStatus(), Status.COMPLETED);
    }

    @Test
    public void listProject() {
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.listProject(new ProjectListRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.listProject(new ProjectListRequest(null, null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.listProject(new ProjectListRequest("", null))
        );
        @NotNull final ProjectListResponse responseEmptyList =
                projectEndpoint.listProject(new ProjectListRequest(tokenTest, null));
        Assert.assertNotNull(responseEmptyList);
        @Nullable List<Project> projectsEmpty = responseEmptyList.getProjects();
        Assert.assertNull(projectsEmpty);
        int countProject = 10;
        @Nullable final List<String> projectIdList = new ArrayList<>();
        for (int i = 0; i < countProject; i++) {
            @NotNull final String projectName = String.format("Project_%d", i);
            @NotNull final ProjectCreateResponse responseProject = projectEndpoint.createProject(
                    new ProjectCreateRequest(tokenTest, projectName, projectDescription, dateBegin, dateEnd)
            );
            Assert.assertNotNull(responseProject);
            @Nullable Project project = responseProject.getProject();
            Assert.assertNotNull(project);
            projectIdList.add(project.getId());
        }

        @NotNull ProjectListResponse response = projectEndpoint.listProject(new ProjectListRequest(tokenTest));
        Assert.assertNotNull(response);
        @Nullable List<Project> projects = response.getProjects();
        Assert.assertNotNull(projects);
        Assert.assertEquals(countProject, projects.size());
        @NotNull List<String> projectIdListCopy = new ArrayList<>(projectIdList);
        for(@NotNull final Project project : projects) {
            @NotNull final String projectId = project.getId();
            Assert.assertTrue(projectIdListCopy.contains(projectId));
            projectIdListCopy.remove(projectId);
        }
        Assert.assertEquals(0, projectIdListCopy.size());

        response = projectEndpoint.listProject(new ProjectListRequest(tokenTest, Sort.BY_NAME));
        Assert.assertNotNull(response);
        projects = response.getProjects();
        Assert.assertNotNull(projects);
        Assert.assertEquals(countProject, projects.size());
        projectIdListCopy = new ArrayList<>(projectIdList);
        int idx = 0;
        for(@NotNull final Project project : projects) {
            @NotNull final String projectId = project.getId();
            Assert.assertTrue(projectIdListCopy.contains(projectId));
            projectIdListCopy.remove(projectId);
            Assert.assertEquals(String.format("Project_%d", idx++), project.getName());
        }
        Assert.assertEquals(0, projectIdListCopy.size());
    }

    @Test
    public void getProjectById() {
        @NotNull final ProjectCreateResponse responseProject = projectEndpoint.createProject(
                new ProjectCreateRequest(tokenTest, projectName, projectDescription, dateBegin, dateEnd)
        );
        Assert.assertNotNull(responseProject);
        @Nullable Project project = responseProject.getProject();
        Assert.assertNotNull(project);
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.getProjectById(
                        new ProjectGetByIdRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.getProjectById(
                        new ProjectGetByIdRequest(null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.getProjectById(
                        new ProjectGetByIdRequest("", null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.getProjectById(
                        new ProjectGetByIdRequest(invalid, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.getProjectById(
                        new ProjectGetByIdRequest(tokenTest, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.getProjectById(
                        new ProjectGetByIdRequest(tokenTest, "")
                )
        );
        @Nullable ProjectGetByIdResponse responseNull = projectEndpoint.getProjectById(
                new ProjectGetByIdRequest(tokenTest, invalid)
        );
        Assert.assertNotNull(responseNull);
        Assert.assertNull(responseNull.getProject());
        @Nullable ProjectGetByIdResponse response = projectEndpoint.getProjectById(
                new ProjectGetByIdRequest(tokenTest, project.getId())
        );
        Assert.assertNotNull(response);
        @Nullable Project projectGet = response.getProject();
        Assert.assertNotNull(projectGet);
        Assert.assertEquals(project.getId(), projectGet.getId());
        Assert.assertEquals(project.getName(), projectGet.getName());
        Assert.assertEquals(project.getDescription(), projectGet.getDescription());
        Assert.assertEquals(project.getDateBegin().toString(), projectGet.getDateBegin().toString());
        Assert.assertEquals(project.getDateEnd().toString(), projectGet.getDateEnd().toString());
    }

    @Test
    public void getProjectByIndex() {
        @NotNull final ProjectCreateResponse responseProject = projectEndpoint.createProject(
                new ProjectCreateRequest(tokenTest, projectName, projectDescription, dateBegin, dateEnd)
        );
        Assert.assertNotNull(responseProject);
        @Nullable Project project = responseProject.getProject();
        Assert.assertNotNull(project);
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.getProjectByIndex(
                        new ProjectGetByIndexRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.getProjectByIndex(
                        new ProjectGetByIndexRequest(null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.getProjectByIndex(
                        new ProjectGetByIndexRequest("", null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.getProjectByIndex(
                        new ProjectGetByIndexRequest(invalid, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.getProjectByIndex(
                        new ProjectGetByIndexRequest(tokenTest, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.getProjectByIndex(
                        new ProjectGetByIndexRequest(tokenTest, -50)
                )
        );
        @Nullable ProjectGetByIndexResponse response = projectEndpoint.getProjectByIndex(
                new ProjectGetByIndexRequest(tokenTest, 1)
        );
        Assert.assertNotNull(response);
        @Nullable Project projectGet = response.getProject();
        Assert.assertNotNull(projectGet);
        Assert.assertEquals(project.getId(), projectGet.getId());
        Assert.assertEquals(project.getName(), projectGet.getName());
        Assert.assertEquals(project.getDescription(), projectGet.getDescription());
        Assert.assertEquals(project.getDateBegin().toString(), projectGet.getDateBegin().toString());
        Assert.assertEquals(project.getDateEnd().toString(), projectGet.getDateEnd().toString());
    }

    @Test
    public void updateProjectById() {
        @NotNull final ProjectCreateResponse responseProject = projectEndpoint.createProject(
                new ProjectCreateRequest(tokenTest, projectName, projectDescription, dateBegin, dateEnd)
        );
        Assert.assertNotNull(responseProject);
        @Nullable Project project = responseProject.getProject();
        Assert.assertNotNull(project);
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectById(
                        new ProjectUpdateByIdRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectById(
                        new ProjectUpdateByIdRequest(null, null, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectById(
                        new ProjectUpdateByIdRequest("", null, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectById(
                        new ProjectUpdateByIdRequest(invalid, null, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectById(
                        new ProjectUpdateByIdRequest(tokenTest, null, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectById(
                        new ProjectUpdateByIdRequest(tokenTest, "", null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectById(
                        new ProjectUpdateByIdRequest(tokenTest, project.getId(), "", null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectById(
                        new ProjectUpdateByIdRequest(tokenTest, project.getId(), invalid, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectById(
                        new ProjectUpdateByIdRequest(tokenTest, project.getId(), invalid, "")
                )
        );
        @NotNull String newName = UUID.randomUUID().toString();
        @NotNull String newDescription = UUID.randomUUID().toString();
        @Nullable ProjectUpdateByIdResponse response = projectEndpoint.updateProjectById(
                new ProjectUpdateByIdRequest(tokenTest, project.getId(), newName, newDescription)
        );
        Assert.assertNotNull(response);
        @Nullable Project projectUpdate = response.getProject();
        Assert.assertNotNull(projectUpdate);
        Assert.assertEquals(project.getId(), projectUpdate.getId());
        Assert.assertEquals(newName, projectUpdate.getName());
        Assert.assertEquals(newDescription, projectUpdate.getDescription());
        Assert.assertEquals(project.getDateBegin().toString(), projectUpdate.getDateBegin().toString());
        Assert.assertEquals(project.getDateEnd().toString(), projectUpdate.getDateEnd().toString());
    }

    @Test
    public void updateProjectByIndex() {
        @NotNull final ProjectCreateResponse responseProject = projectEndpoint.createProject(
                new ProjectCreateRequest(tokenTest, projectName, projectDescription, dateBegin, dateEnd)
        );
        Assert.assertNotNull(responseProject);
        @Nullable Project project = responseProject.getProject();
        Assert.assertNotNull(project);
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectByIndex(
                        new ProjectUpdateByIndexRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectByIndex(
                        new ProjectUpdateByIndexRequest(null, null, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectByIndex(
                        new ProjectUpdateByIndexRequest("", null, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectByIndex(
                        new ProjectUpdateByIndexRequest(invalid, null, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectByIndex(
                        new ProjectUpdateByIndexRequest(tokenTest, null, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectByIndex(
                        new ProjectUpdateByIndexRequest(tokenTest, -50, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectByIndex(
                        new ProjectUpdateByIndexRequest(tokenTest, 50, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectByIndex(
                        new ProjectUpdateByIndexRequest(tokenTest, 1, "", null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectByIndex(
                        new ProjectUpdateByIndexRequest(tokenTest, 1, invalid, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectByIndex(
                        new ProjectUpdateByIndexRequest(tokenTest, 1, invalid, "")
                )
        );
        @NotNull String newName = UUID.randomUUID().toString();
        @NotNull String newDescription = UUID.randomUUID().toString();
        @Nullable ProjectUpdateByIndexResponse response = projectEndpoint.updateProjectByIndex(
                new ProjectUpdateByIndexRequest(tokenTest, 1, newName, newDescription)
        );
        Assert.assertNotNull(response);
        @Nullable Project projectUpdate = response.getProject();
        Assert.assertNotNull(projectUpdate);
        Assert.assertEquals(project.getId(), projectUpdate.getId());
        Assert.assertEquals(newName, projectUpdate.getName());
        Assert.assertEquals(newDescription, projectUpdate.getDescription());
        Assert.assertEquals(project.getDateBegin().toString(), projectUpdate.getDateBegin().toString());
        Assert.assertEquals(project.getDateEnd().toString(), projectUpdate.getDateEnd().toString());
    }

}
