package ru.tsc.kirillov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.kirillov.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.kirillov.tm.api.endpoint.IUserEndpoint;
import ru.tsc.kirillov.tm.api.service.IPropertyService;
import ru.tsc.kirillov.tm.dto.request.*;
import ru.tsc.kirillov.tm.dto.response.UserLoginResponse;
import ru.tsc.kirillov.tm.dto.response.UserProfileResponse;
import ru.tsc.kirillov.tm.dto.response.UserRegistryResponse;
import ru.tsc.kirillov.tm.dto.response.UserUpdateProfileResponse;
import ru.tsc.kirillov.tm.marker.ISoapCategory;
import ru.tsc.kirillov.tm.model.User;
import ru.tsc.kirillov.tm.service.PropertyService;

import java.util.UUID;

@Category(ISoapCategory.class)
public final class UserEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @Nullable
    private String tokenAdmin;

    @Nullable
    private String tokenTest;

    @NotNull
    private final String invalid = UUID.randomUUID().toString();

    @Nullable
    private final String userLogin = UUID.randomUUID().toString();

    @Nullable
    private final String userPassword = UUID.randomUUID().toString();

    @Nullable
    private final String userEmail = UUID.randomUUID().toString();

    @Before
    public void initialization() {
        @NotNull final UserLoginResponse responseAdmin = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        tokenAdmin = responseAdmin.getToken();
        @NotNull final UserLoginResponse responseTest = authEndpoint.login(
                new UserLoginRequest("test", "test")
        );
        tokenTest = responseTest.getToken();
        userEndpoint.unlockUser(new UserUnlockRequest(tokenAdmin, "test"));
    }

    @After
    public void finalization() {
        tokenAdmin = null;
    }

    @Test
    public void lockUser() {
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.lockUser(new UserLockRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.lockUser(new UserLockRequest(null, null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.lockUser(new UserLockRequest("", ""))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.lockUser(new UserLockRequest(invalid, null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.lockUser(new UserLockRequest(tokenAdmin, null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.lockUser(new UserLockRequest(tokenAdmin, ""))
        );
        userEndpoint.lockUser(new UserLockRequest(tokenAdmin, "test"));
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("test", "test"))
        );
    }

    @Test
    public void unlockUser() {
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.unlockUser(new UserUnlockRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.unlockUser(new UserUnlockRequest(null, null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.unlockUser(new UserUnlockRequest("", ""))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.unlockUser(new UserUnlockRequest(invalid, null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.unlockUser(new UserUnlockRequest(tokenAdmin, null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.unlockUser(new UserUnlockRequest(tokenAdmin, ""))
        );
        userEndpoint.lockUser(new UserLockRequest(tokenAdmin, "test"));
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("test", "test"))
        );
        userEndpoint.unlockUser(new UserUnlockRequest(tokenAdmin, "test"));
        @NotNull final UserLoginResponse response = authEndpoint.login(
                new UserLoginRequest("test", "test")
        );
        @Nullable String token = response.getToken();
        Assert.assertNotNull(token);
    }

    @Test
    public void registryUser() {
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.registryUser(new UserRegistryRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.registryUser(new UserRegistryRequest(null, null, null, null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.registryUser(new UserRegistryRequest("", "", "", ""))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.registryUser(new UserRegistryRequest("", userLogin, null, null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.registryUser(new UserRegistryRequest("", userLogin, "", null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.registryUser(new UserRegistryRequest("", userLogin, userPassword, null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.registryUser(new UserRegistryRequest("", userLogin, userPassword, ""))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.registryUser(new UserRegistryRequest("", "admin", userPassword, userEmail))
        );
        @NotNull UserRegistryResponse response =
                userEndpoint.registryUser(new UserRegistryRequest("", userLogin, userPassword, userEmail));
        @Nullable User user = response.getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(userLogin, user.getLogin());
        Assert.assertEquals(userEmail, user.getEmail());
        @NotNull final UserLoginResponse responseLogin = authEndpoint.login(
                new UserLoginRequest(userLogin, userPassword)
        );
        @Nullable String token = responseLogin.getToken();
        Assert.assertNotNull(token);
        userEndpoint.removeUser(new UserRemoveRequest(tokenAdmin, userLogin));
    }

    @Test
    public void removeUser() {
        userEndpoint.registryUser(new UserRegistryRequest("", userLogin, userPassword, userEmail));
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.removeUser(new UserRemoveRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.removeUser(new UserRemoveRequest(null, null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.removeUser(new UserRemoveRequest("", null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.removeUser(new UserRemoveRequest(tokenTest, null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.removeUser(new UserRemoveRequest(tokenTest, ""))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.removeUser(new UserRemoveRequest(tokenTest, userLogin))
        );
        userEndpoint.removeUser(new UserRemoveRequest(tokenAdmin, userLogin));
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.login(new UserLoginRequest(userLogin, userPassword))
        );
    }

    @Test
    public void updateProfileUser() {
        userEndpoint.registryUser(new UserRegistryRequest("", userLogin, userPassword, userEmail));
        @NotNull final UserLoginResponse responseLogin = authEndpoint.login(
                new UserLoginRequest(userLogin, userPassword)
        );
        @Nullable String token = responseLogin.getToken();
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.updateProfileUser(
                        new UserUpdateProfileRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.updateProfileUser(
                        new UserUpdateProfileRequest(null, null, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.updateProfileUser(
                        new UserUpdateProfileRequest("", null, null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.updateProfileUser(
                        new UserUpdateProfileRequest(invalid, null, null, null)
                )
        );
        @NotNull final String firstName = UUID.randomUUID().toString();
        @NotNull final String lastName = UUID.randomUUID().toString();
        @NotNull final String middleName = UUID.randomUUID().toString();
        @NotNull UserUpdateProfileResponse response =
                userEndpoint.updateProfileUser(new UserUpdateProfileRequest(token, firstName, lastName, middleName));
        @Nullable User user = response.getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(firstName, user.getFirstName());
        Assert.assertEquals(lastName, user.getLastName());
        Assert.assertEquals(middleName, user.getMiddleName());
        userEndpoint.removeUser(new UserRemoveRequest(tokenAdmin, userLogin));
    }

    @Test
    public void viewProfileUser() {
        userEndpoint.registryUser(new UserRegistryRequest("", userLogin, userPassword, userEmail));
        @NotNull final UserLoginResponse responseLogin = authEndpoint.login(
                new UserLoginRequest(userLogin, userPassword)
        );
        @Nullable String token = responseLogin.getToken();
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.viewProfileUser(
                        new UserProfileRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.viewProfileUser(
                        new UserProfileRequest(null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.viewProfileUser(
                        new UserProfileRequest("")
                )
        );
        @NotNull UserProfileResponse response = userEndpoint.viewProfileUser(new UserProfileRequest(token));
        @Nullable User user = response.getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(userLogin, user.getLogin());
        userEndpoint.removeUser(new UserRemoveRequest(tokenAdmin, userLogin));
    }

    @Test
    public void changePassword() {
        userEndpoint.registryUser(new UserRegistryRequest("", userLogin, userPassword, userEmail));
        @NotNull UserLoginResponse responseLogin = authEndpoint.login(
                new UserLoginRequest(userLogin, userPassword)
        );
        @Nullable final String token = responseLogin.getToken();
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.changePassword(
                        new UserChangePasswordRequest()
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.changePassword(
                        new UserChangePasswordRequest(null, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.changePassword(
                        new UserChangePasswordRequest("", null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.changePassword(
                        new UserChangePasswordRequest(token, null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.changePassword(
                        new UserChangePasswordRequest(token, "")
                )
        );
        @NotNull String newUserPassword = UUID.randomUUID().toString();
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.login(new UserLoginRequest(userLogin, newUserPassword))
        );
        userEndpoint.changePassword(new UserChangePasswordRequest(token, newUserPassword));
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.login(new UserLoginRequest(userLogin, userPassword))
        );
        responseLogin = authEndpoint.login(
                new UserLoginRequest(userLogin, newUserPassword)
        );
        @Nullable final String tokenChange = responseLogin.getToken();
        Assert.assertNotNull(tokenChange);
        userEndpoint.removeUser(new UserRemoveRequest(tokenAdmin, userLogin));
    }

}
