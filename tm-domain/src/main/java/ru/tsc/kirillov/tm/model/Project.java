package ru.tsc.kirillov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Project extends AbstractUserOwnedModel {

    private static final long serialVersionUID = 1;

    public Project(@NotNull final String userId, @NotNull final String name) {
        super(userId, name);
    }

    public Project(@NotNull final String userId, @NotNull final String name, @Nullable final String description) {
        super(userId, name, description);
    }

    public Project(@NotNull final String name, @NotNull final Status status, @Nullable final Date dateBegin) {
        super(name, status, dateBegin);
    }

    public Project(
            @NotNull final String userId,
            @NotNull final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        super(userId, name, description, dateBegin, dateEnd);
    }
}
