package ru.tsc.kirillov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.IUserOwnedRepository;
import ru.tsc.kirillov.tm.api.service.IConnectionService;
import ru.tsc.kirillov.tm.api.service.IUserOwnedService;
import ru.tsc.kirillov.tm.enumerated.Sort;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R>
        implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Nullable
    @Override
    public abstract M createName(@Nullable final String userId, @Nullable final String name);

    @Nullable
    @Override
    public abstract M createNameDescription(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    );

    @Nullable
    @Override
    public abstract M createAll(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    );

    @Override
    public abstract void clearUserId(@Nullable final String userId);

    @NotNull
    @Override
    public abstract List<M> findAllUserId(@Nullable final String userId);

    @NotNull
    @Override
    public abstract List<M> findAllComparatorUserId(@Nullable final String userId, @Nullable final Comparator<M> comparator);

    @NotNull
    @Override
    public abstract List<M> findAllSortUserId(@Nullable final String userId, @Nullable final Sort sort);

    @Override
    public abstract boolean existsByIdUserId(@Nullable final String userId, @Nullable final String id);

    @Nullable
    @Override
    public abstract M findOneByIdUserId(@Nullable final String userId, @Nullable final String id);

    @Nullable
    @Override
    public abstract M findOneByIndexUserId(@Nullable final String userId, @Nullable final Integer index);

    @Override
    public abstract int removeUserId(@Nullable final String userId, @Nullable final M model);

    @Override
    public abstract int removeByIdUserId(@Nullable final String userId, @Nullable final String id);

    @Override
    public abstract int removeByIndexUserId(@Nullable final String userId, @Nullable final Integer index);

    @Override
    public abstract int update(@Nullable final M model);

    @Nullable
    @Override
    public abstract M updateByIdUserId(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    );

    @Nullable
    @Override
    public abstract M updateByIndexUserId(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    );

    @Nullable
    @Override
    public abstract M changeStatusByIdUserId(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    );

    @Nullable
    @Override
    public abstract M changeStatusByIndexUserId(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    );

    @Override
    public abstract long countUserId(@Nullable final String userId);

}
