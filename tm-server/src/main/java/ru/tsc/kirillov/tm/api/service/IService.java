package ru.tsc.kirillov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.repository.IRepository;
import ru.tsc.kirillov.tm.model.AbstractModel;

import java.util.Collection;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

}
