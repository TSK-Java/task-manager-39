package ru.tsc.kirillov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.IRepository;
import ru.tsc.kirillov.tm.api.service.IConnectionService;
import ru.tsc.kirillov.tm.api.service.IService;
import ru.tsc.kirillov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected SqlSession getSession() {
        return connectionService.getSqlSession();
    }

    @Override
    public abstract void add(@Nullable final M model);

    @Override
    public abstract void addAll(@NotNull final Collection<M> models);

    @NotNull
    public abstract Collection<M> set(@NotNull final Collection<M> models);

    @Override
    public abstract void clear();

    @NotNull
    @Override
    public abstract List<M> findAll();

    @Override
    public abstract boolean existsById(@Nullable final String id);

    @Nullable
    @Override
    public abstract M findOneById(@Nullable final String id);

    @Nullable
    @Override
    public abstract M findOneByIndex(@Nullable final Integer index);

    @Override
    public abstract int remove(@Nullable final M model);

    @Override
    public abstract void removeAll(@Nullable final Collection<M> collection);

    @Override
    public abstract int removeById(@Nullable final String id);

    @Override
    public abstract int removeByIndex(@Nullable final Integer index);

    @Override
    public abstract int update(@Nullable final M model);

    @Override
    public abstract long count();

}
