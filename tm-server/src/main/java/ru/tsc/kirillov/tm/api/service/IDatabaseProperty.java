package ru.tsc.kirillov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull
    String getDatabaseUserName();

    @NotNull
    String getDatabaseUserPassword();

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseDriver();

}
