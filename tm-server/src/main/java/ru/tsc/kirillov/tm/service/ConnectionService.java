package ru.tsc.kirillov.tm.service;

import org.apache.ibatis.datasource.unpooled.UnpooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.repository.IProjectRepository;
import ru.tsc.kirillov.tm.api.repository.ITaskRepository;
import ru.tsc.kirillov.tm.api.repository.IUserRepository;
import ru.tsc.kirillov.tm.api.service.IConnectionService;
import ru.tsc.kirillov.tm.api.service.IDatabaseProperty;

import javax.sql.DataSource;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IDatabaseProperty databaseProperty;

    public ConnectionService(@NotNull final IDatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
    }

    @NotNull
    @Override
    public SqlSession getSqlSession() {
        return getSqlSessionFactory().openSession();
    }

    @NotNull
    @Override
    public SqlSessionFactory getSqlSessionFactory() {
        @NotNull final String userName = databaseProperty.getDatabaseUserName();
        @NotNull final String userPassword = databaseProperty.getDatabaseUserPassword();
        @NotNull final String url = databaseProperty.getDatabaseUrl();
        @NotNull final String driver = databaseProperty.getDatabaseDriver();
        @NotNull final DataSource dataSource = new UnpooledDataSource(driver, url, userName, userPassword);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("development", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        configuration.addMapper(IUserRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

}
