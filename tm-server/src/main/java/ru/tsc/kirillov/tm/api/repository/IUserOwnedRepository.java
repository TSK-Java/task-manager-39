package ru.tsc.kirillov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void clearUserId(@Nullable String userId);

    @NotNull
    List<M> findAllUserId(@Nullable String userId);

    @NotNull
    List<M> findAllComparatorUserId(@Nullable String userId, @NotNull Comparator<M> comparator);

    boolean existsByIdUserId(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneByIdUserId(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneByIndexUserId(@Nullable String userId, @Nullable Integer index);

    int removeUserId(@Nullable String userId, @Nullable M model);

    long countUserId(@Nullable String userId);

}
