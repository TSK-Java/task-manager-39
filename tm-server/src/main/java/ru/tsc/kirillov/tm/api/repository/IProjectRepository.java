package ru.tsc.kirillov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import ru.tsc.kirillov.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @Insert("INSERT INTO project (id, created, name, description, status, user_id, date_begin, date_end)"
            + "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId}, #{dateBegin}, #{dateEnd})")
    void add(@NotNull Project model);

    @Insert("INSERT INTO project (id, created, name, description, status, user_id, date_begin, date_end)"
            + "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId}, #{dateBegin}, #{dateEnd})")
    void addAll(@NotNull Collection<Project> models);

    @Delete("DELETE FROM project")
    void clear();

    @Delete("DELETE FROM project WHERE user_id = #{userId}")
    void clearUserId(@Param("userId") @Nullable String userId);

    @NotNull
    @Select("SELECT id, created, name, description, status, user_id, date_begin, date_end FROM project")
    @Results(value = {
         @Result(property = "userId", column = "user_id"),
         @Result(property = "dateBegin", column = "date_begin"),
         @Result(property = "dateEnd", column = "date_end")
    })
    List<Project> findAll();

    @NotNull
    @Select("SELECT id, created, name, description, status, user_id, date_begin, date_end FROM project "
            + "WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    List<Project> findAllUserId(@Param("userId") @Nullable String userId);

    @NotNull
    @SelectProvider(type = SqlProviderAdapter.class, method = "select")
    List<Project> findAllComparator(SelectStatementProvider selectStatementProvider);

    @NotNull
    @SelectProvider(type = SqlProviderAdapter.class, method = "select")
    List<Project> findAllComparatorUserId(SelectStatementProvider selectStatementProvider);

    @Select("SELECT count(1) = 1 FROM project WHERE id = #{id}")
    boolean existsById(@Param("id") @NotNull String id);

    @Select("SELECT count(1) = 1 FROM project WHERE user_id = #{userId} AND id = #{id}")
    boolean existsByIdUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Nullable
    @Select("SELECT id, created, name, description, status, user_id, date_begin, date_end FROM project "
            + "WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    Project findOneById(@Param("id") @NotNull String id);

    @Nullable
    @Select("SELECT id, created, name, description, status, user_id, date_begin, date_end FROM project "
            + "WHERE user_id = #{userId} AND id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    Project findOneByIdUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Nullable
    @Select("SELECT id, created, name, description, status, user_id, date_begin, date_end FROM project LIMIT #{index}, 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    Project findOneByIndex(@Param("index") @NotNull Integer index);

    @Nullable
    @Select("SELECT id, created, name, description, status, user_id, date_begin, date_end FROM project "
            + "WHERE user_id = #{userId} LIMIT #{index}, 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    Project findOneByIndexUserId(@Param("userId") @Nullable String userId, @Param("index") @Nullable Integer index);

    @Delete("DELETE FROM project WHERE id = #{id}")
    int remove(@NotNull Project model);

    @Delete("DELETE FROM project WHERE user_id = #{userId} AND id = #{id}")
    void removeAll(@Nullable Collection<Project> collection);
    
    @Update("UPDATE project SET created = #{created}, name = #{name}, description = #{description}, "
            + "status = #{status}, date_begin = #{dateBegin}, date_end = #{dateEnd} "
            + "WHERE id = #{id}")
    int update(@NotNull Project model);

    @NotNull
    @Select("SELECT id FROM project WHERE user_id = #{userId}")
    String[] findAllId(@Param("userId") @Nullable String userId);

    @Select("SELECT count(1) FROM project")
    long count();

    @Select("SELECT count(1) FROM project WHERE user_id = #{userId}")
    long countUserId(@Param("userId") @Nullable String userId);

}
