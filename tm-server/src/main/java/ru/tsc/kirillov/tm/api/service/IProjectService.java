package ru.tsc.kirillov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    String[] findAllId(@Nullable final String userId);

}
