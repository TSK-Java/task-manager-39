package ru.tsc.kirillov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.IUserOwnedRepository;
import ru.tsc.kirillov.tm.enumerated.Sort;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.model.AbstractUserOwnedModel;

import java.util.Date;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

    @Nullable
    M createName(@Nullable String userId, @Nullable String name);

    @Nullable
    M createNameDescription(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    M createAll(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    );

    @NotNull
    List<M> findAllSortUserId(@Nullable String userId, @Nullable Sort sort);

    @Nullable
    M updateByIdUserId(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    M updateByIndexUserId(
            @Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description
    );

    @Nullable
    M changeStatusByIdUserId(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @Nullable
    M changeStatusByIndexUserId(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    int removeByIdUserId(@Nullable String userId, @Nullable String id);

    int removeByIndexUserId(@Nullable String userId, @Nullable Integer index);

}
