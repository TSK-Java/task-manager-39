package ru.tsc.kirillov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.SortSpecification;
import org.mybatis.dynamic.sql.render.RenderingStrategies;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import ru.tsc.kirillov.tm.api.repository.IProjectRepository;
import ru.tsc.kirillov.tm.api.service.IConnectionService;
import ru.tsc.kirillov.tm.api.service.IProjectService;
import ru.tsc.kirillov.tm.comparator.CreatedComparator;
import ru.tsc.kirillov.tm.comparator.DateBeginComparator;
import ru.tsc.kirillov.tm.comparator.StatusComparator;
import ru.tsc.kirillov.tm.enumerated.Sort;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.exception.field.*;
import ru.tsc.kirillov.tm.exception.system.IndexOutOfBoundsException;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.model.ProjectProvider;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static org.mybatis.dynamic.sql.SqlBuilder.isEqualTo;
import static org.mybatis.dynamic.sql.SqlBuilder.select;
import static ru.tsc.kirillov.tm.model.ProjectProvider.*;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    private SortSpecification getSortColumnName(@Nullable final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return created;
        else if (comparator == StatusComparator.INSTANCE) return status;
        else if (comparator == DateBeginComparator.INSTANCE) return dateBegin;
        else return name;
    }
    
    @Override
    public void add(@Nullable final Project model) {
        if (model == null) return;
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.add(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void addAll(@NotNull final Collection<Project> models) {
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.addAll(models);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Project createName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final SqlSession sqlSession = getSession();
        @NotNull final Project model;
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            model = new Project(userId, name);
            repository.add(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return model;
    }

    @NotNull
    @Override
    public Project createNameDescription(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable Project model;
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            model = new Project(userId, name, description);
            repository.add(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return model;
    }

    @NotNull
    @Override
    public Project createAll(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable Project model;
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            model = new Project(userId, name, description, dateBegin, dateEnd);
            repository.add(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return model;
    }

    @NotNull
    @Override
    public Collection<Project> set(@NotNull final Collection<Project> models) {
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.clear();
            repository.addAll(models);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return models;
    }

    @Override
    public void clear() {
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clearUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.clearUserId(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.findAll();
        }
    }

    @NotNull
    @Override
    public List<Project> findAllUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.findAllUserId(userId);
        }
    }

    @NotNull
    public List<Project> findAllSort(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return findAllComparator(sort.getComparator());
    }

    @NotNull
    @Override
    
    public List<Project> findAllSortUserId(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAllUserId(userId);
        return findAllComparatorUserId(userId, sort.getComparator());
    }

    @NotNull
    public List<Project> findAllComparator(@Nullable final Comparator<Project> comparator) {
        if (comparator == null) return findAll();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull final SelectStatementProvider selectStatementProvider =
                    select(id, userId, name, description, status, created, dateBegin, dateEnd)
                            .from(project)
                            .orderBy(getSortColumnName(comparator))
                            .build()
                            .render(RenderingStrategies.MYBATIS3);
            return repository.findAllComparator(selectStatementProvider);
        }
    }

    @NotNull
    @Override
    public List<Project> findAllComparatorUserId(@Nullable final String userId, @Nullable final Comparator<Project> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAllUserId(userId);
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull final SelectStatementProvider selectStatementProvider =
                    select(id, ProjectProvider.userId, name, description, status, created, dateBegin, dateEnd)
                            .from(project)
                            .where(ProjectProvider.userId, isEqualTo(userId))
                            .orderBy(getSortColumnName(comparator))
                            .build()
                            .render(RenderingStrategies.MYBATIS3);
            return repository.findAllComparator(selectStatementProvider);
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.existsById(id);
        }
    }

    @Override
    public boolean existsByIdUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.existsByIdUserId(userId, id);
        }
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.findOneById(id);
        }
    }

    @Nullable
    @Override
    public Project findOneByIdUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.findOneByIdUserId(userId, id);
        }
    }

    @Nullable
    @Override
    public Project findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexOutOfBoundsException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.findOneByIndex(index);
        }
    }

    @Nullable
    @Override
    public Project findOneByIndexUserId(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexOutOfBoundsException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.findOneByIndexUserId(userId, index);
        }
    }

    @Override
    public int remove(@Nullable final Project model) {
        int result = 0;
        if (model == null) return result;
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            result = repository.remove(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public int removeUserId(@Nullable final String userId, @Nullable final Project model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        int result = 0;
        if (model == null) return result;
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            result = repository.removeUserId(userId, model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public void removeAll(@Nullable final Collection<Project> collection) {
        if (collection == null || collection.isEmpty()) return;
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.removeAll(collection);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public int removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = getSession();
        @Nullable final Project project;
        int result = 0;
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            project = repository.findOneById(id);
            if (project == null) return result;
            result = repository.remove(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public int removeByIdUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = getSession();
        @Nullable final Project project;
        int result = 0;
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            project = repository.findOneByIdUserId(userId, id);
            if (project == null) return result;
            result = repository.remove(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public int removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexOutOfBoundsException();
        @NotNull final SqlSession sqlSession = getSession();
        @Nullable final Project project;
        int result = 0;
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            project = repository.findOneByIndex(index);
            if (project == null) return result;
            result = repository.remove(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public int removeByIndexUserId(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexOutOfBoundsException();
        @NotNull final SqlSession sqlSession = getSession();
        @Nullable final Project project;
        int result = 0;
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            project = repository.findOneByIndexUserId(userId, index);
            if (project == null) return result;
            result = repository.remove(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public int update(@Nullable final Project model) {
        int result = 0;
        if (model == null) return result;
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            result = repository.update(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Nullable
    @Override
    public Project updateByIdUserId(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final SqlSession sqlSession = getSession();
        @Nullable final Project model;
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            model = repository.findOneByIdUserId(userId, id);
            if (model == null) return null;
            model.setName(name);
            model.setDescription(description);
            repository.update(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return model;
    }

    @Nullable
    @Override
    public Project updateByIndexUserId(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexOutOfBoundsException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final SqlSession sqlSession = getSession();
        @Nullable final Project model;
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            model = repository.findOneByIndexUserId(userId, index);
            if (model == null) return null;
            model.setName(name);
            model.setDescription(description);
            repository.update(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return model;
    }

    @Nullable
    @Override
    public Project changeStatusByIdUserId(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final SqlSession sqlSession = getSession();
        @Nullable final Project model;
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            model = repository.findOneByIdUserId(userId, id);
            if (model == null) return null;
            model.setStatus(status);
            repository.update(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return model;
    }

    @Nullable
    @Override
    public Project changeStatusByIndexUserId(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexOutOfBoundsException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final SqlSession sqlSession = getSession();
        @Nullable final Project model;
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            model = findOneByIndexUserId(userId, index);
            if (model == null) return null;
            model.setStatus(status);
            repository.update(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return model;
    }

    @Override
    public long count() {
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.count();
        }
    }

    @Override
    public long countUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.countUserId(userId);
        }
    }

    @NotNull
    @Override
    public String[] findAllId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.findAllId(userId);
        }
    }

}
